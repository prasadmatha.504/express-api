const express = require('express') 
const app = express()
const redis = require('redis')
require('dotenv').config()
const bluebird = require('bluebird')
bluebird.promisifyAll(redis)
const  RedisClient = redis.createClient()
RedisClient.on("error",function(err){
    console.log(err.message);
});
RedisClient.on("ready",function(err){
    console.log("connected");
});


app.use(express.json())

let regex = {
    "id" : /^[0-9]+$/,
    "name": /^[a-zA-Z ]+$/,
    "score":/^[0-9]+$/,
    "email": /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
}

//validate request body
function validateData(requestBody){
    let invalidFields = []
    let mandatoryFields = ["name","score","email"]
    let attributesInRequestBody = Object.keys(requestBody)
    mandatoryFields.forEach(eachAttribute=>{
        if(attributesInRequestBody.includes(eachAttribute))
        {
            if(!requestBody[eachAttribute] || !regex[eachAttribute].test(requestBody[eachAttribute])) invalidFields.push(eachAttribute)
        }
        else{
            invalidFields.push(eachAttribute)
        }
    })
    return invalidFields.length == 0 ? true : invalidFields
}

function getData(){
    return RedisClient.getAsync("data").then(res=>res).catch(err=>err)
}

//get all the user details
app.get("/",async (req,res)=>{ 
     getData().then(data=>{
        data = JSON.parse(data)
        if(data){
            res.status(200).send({"isSuccessful" : true, "totalUsers" : data.length, "message" : "Received User's details successfully", "response" : data})
        }
        else{
            res.status(404).send({"isSuccessful" : false, "message" : "User's details are not found! So unable to fetch data"})
        }
    }).catch(err=>{
        console.error(`Error :: ${err}`)
    })
})

function validateIdentifierAndValue(identifier,identifierValue){
    let identifiers = ["id","name","score","email"]
    let isValidIdentifierValue = identifier && identifiers.includes(identifier) && regex[identifier].test(identifierValue) ? true : false
    return isValidIdentifierValue
}

//get specific user details
app.get("/search",async (req,res)=>{  
    let identifier = Object.keys(req.query)[0]
    let identifierValue = req.query[identifier]
    getData().then(data=>{
        if(data){
            data = JSON.parse(data)
            let user = []
            if(validateIdentifierAndValue(identifier,identifierValue)){
                user = data.filter(user => {
                    if(isNaN(user[identifier])){
                        return user[identifier].toLowerCase() == identifierValue.toLowerCase()
                    }
                    return user[identifier] == identifierValue
                })
                if(user.length >= 1){
                    return res.status(200).send({"isSuccessful": true, "totalUsers" : user.length,"message": "Received user's details successfully", "response" : user})
                }
                return res.status(404).send({"isSuccessful": false, "message" : `No user is found with the given ${identifier} :: ${identifierValue}` })
            }
            else{
                return res.status(400).send({"isSuccessful": false, "message" : `Please provide valid key & value pair to search an user.` })
            }
        }
        else{
            return res.status(404).send({"isSuccessful": false, "message" : `User's details are not found! So unable to fetch data` })
        }   
    }).catch(err=>{
        console.error(`Error :: ${err}`)
    })    
})

app.get("/sort",async (req,res)=>{ 
    let order = req.query.order
    getData().then(data=>{
       data = JSON.parse(data)
       if(data){
            if (order == "1"){
                data = data.sort((prev,curr)=>{
                    if (prev.score < curr.score) return -1
                    else if (prev.score > curr.score) return 1
                    else return 0
                })
            }
            else if(order == "0"){
                data = data.sort((prev,curr)=>{
                    if (prev.score < curr.score) return 1
                    else if (prev.score > curr.score) return -1
                    else return 0
                })
            }
           res.status(200).send({"isSuccessful" : true, "totalUsers" : data.length, "message" : "Received User's details successfully", "response" : data})
       }
       else{
           res.status(404).send({"isSuccessful" : false, "message" : "User's details are not found! So unable to fetch data"})
       }
   }).catch(err=>{
       console.error(`Error :: ${err}`)
   })
})

app.get("/heighest",async (req,res)=>{ 
    getData().then(data=>{
       data = JSON.parse(data)
       scores = []
       if(data){
            data.forEach(each=>{
                let score = typeof each.score == "string" ? scores.push(parseInt(each.score)) : scores.push(each.score)
            })
            let maxValue = Math.max(...scores)
            let users = data.filter(each=>{
                return each.score == maxValue
            })
           res.status(200).send({"isSuccessful" : true, "totalUsers" : users.length, "message" : "Received User's details successfully", "response" : users})
       }
       else{
           res.status(404).send({"isSuccessful" : false, "message" : "User's details are not found! So unable to fetch data"})
       }
   }).catch(err=>{
       console.error(`Error :: ${err}`)
   })
})

app.get("/lowest",async (req,res)=>{ 
    getData().then(data=>{
       data = JSON.parse(data)
       scores = []
       if(data){
            data.forEach(each=>{
                let score = typeof each.score == "string" ? scores.push(parseInt(each.score)) : scores.push(each.score)
            })
            let minValue = Math.min(...scores)
            let users = data.filter(each=>{
                return each.score == minValue
            })
           res.status(200).send({"isSuccessful" : true, "totalUsers" : users.length, "message" : "Received User's details successfully", "response" : users})
       }
       else{
           res.status(404).send({"isSuccessful" : false, "message" : "User's details are not found! So unable to fetch data"})
       }
   }).catch(err=>{
       console.error(`Error :: ${err}`)
   })
})

//create an user
app.post("/",(req,res)=>{ 
    getData().then(data=>{
    data = data == null ? [] : JSON.parse(data)
    let body = req.body
    if(validateData(body) == true){
        let id = data.length > 0 ? data[data.length-1].id + 1 : 1 
        let {email} = body
        let existedUser = data.find(user => user.email == email)
        if(!existedUser){
            body = {"id":id, ...body}
            data.push(body)
            RedisClient.set("data", JSON.stringify(data))
            return res.status(200).send({ "id" : id, "isSuccessful": true, "message": `User is created successfully.`})
        }
        return res.status(400).send({"isSuccessful" : false, "message" : "Unable to create user ===> EmailID already exists"})
    }
    return res.status(400).send({"isSuccessful": false, "message": `Fields should be provided with valid data ===> ${validateData(body).join(", ")}`})
    }).catch(err=>{
        console.error(`Error :: ${err}`)
    })
})

const PORT = process.env.PORT || 3000
app.listen(PORT, console.log(`Server is running on port ${PORT}`))